import { configureStore } from '@reduxjs/toolkit';
import ListOfContactReducer from '../reducer/ListOfContactReducer';
import DetailOfContactReducer from '../reducer/DetailOfContactReducer';
import CreateContactReducer from '../reducer/CreateContactReducer';
import UpdateContactReducer from '../reducer/UpdateContactReducer';
import DeleteContactReducer from '../reducer/DeleteContactReducer';
import ShowDeleteVisibilityReducer from '../reducer/ShowDeleteVisibilityReducer';
import ReloadPageReducer from '../reducer/ReloadPageReducer';

const store = configureStore({
    reducer: {
        listOfcontact: ListOfContactReducer,
        detailOfContact: DetailOfContactReducer,
        createContact: CreateContactReducer,
        updateContact: UpdateContactReducer,
        deleteContact: DeleteContactReducer,
        showDeleteVisibility: ShowDeleteVisibilityReducer,
        reloadPage: ReloadPageReducer
    }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;