export interface StandardResponse {
    message: string;
    data:   any;
}

// Converts JSON strings to/from your types
export class ConvertStandardResponse {
    public static toModel(json: string): StandardResponse {
        return JSON.parse(json);
    }

    public static toJson(value: StandardResponse): string {
        return JSON.stringify(value);
    }
}
