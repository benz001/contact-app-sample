// To parse this data:
//
//   import { Convert, ListOfContactResponse } from "./file";
//
//   const listOfContactResponse = Convert.toListOfContactResponse(json);

export interface ListOfContactResponse {
    message: string;
    data:    DataOfContact[];
}

export interface DataOfContact {
    id:        string;
    firstName: string;
    lastName:  string;
    age:       number;
    photo:     string;
}

// Converts JSON strings to/from your types
export class ConvertListOfContactResponse {
    public static toModel(json: string): ListOfContactResponse {
        return JSON.parse(json);
    }

    public static toJson(value: ListOfContactResponse): string {
        return JSON.stringify(value);
    }
}
