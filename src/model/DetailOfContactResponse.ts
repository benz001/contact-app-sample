// To parse this data:
//
//   import { Convert, DetailOfContactResponse } from "./file";
//
//   const detailOfContactResponse = Convert.toDetailOfContactResponse(json);

export interface DetailOfContactResponse {
    message: string;
    data:    DataOfContact;
}

export interface DataOfContact {
    id:        string;
    firstName: string;
    lastName:  string;
    age:       number;
    photo:     string;
}

// Converts JSON strings to/from your types
export class ConvertDetailOfContactResponse {
    public static toModel(json: string): DetailOfContactResponse {
        return JSON.parse(json);
    }

    public static toJson(value: DetailOfContactResponse): string {
        return JSON.stringify(value);
    }
}
