import { createSlice } from "@reduxjs/toolkit";
import { SET_ERROR, SET_LOADING, SET_SUCCESS } from "../helper/RequestState";

const updateContactSlice = createSlice({
    name: 'updateContact',
    initialState: {
        isLoading: false,
        isSuccess: false,
        data: null
    },
    reducers: {
        setStateUpdateContact: (state, action) => {
            switch (action.payload.type) {
                case SET_LOADING:
                    return {
                        isLoading: true,
                        isSuccess: false,
                        data: null
                    }
                case SET_SUCCESS:
                    return {
                        isLoading: false,
                        isSuccess: true,
                        data: null
                    }
                case SET_ERROR:
                    return {
                        isLoading: false,
                        isSuccess: false,
                        data: null
                    }
                default:
                    return state;
            }
        }
    }
});

export const { setStateUpdateContact } = updateContactSlice.actions;


export default updateContactSlice.reducer;