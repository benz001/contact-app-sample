import { createSlice } from "@reduxjs/toolkit";
import { SET_ERROR, SET_LOADING, SET_SUCCESS } from "../helper/RequestState";

const detailOfContactSlice = createSlice({
    name: 'detailOfcontact',
    initialState: {
        isLoading: false,
        isSuccess: false,
        data: null
    },
    reducers: {
        setStateDetailOfContact: (state, action) => {
            switch (action.payload.type) {
                case SET_LOADING:
                    return {
                        isLoading: true,
                        isSuccess: false,
                        data: null
                    }
                case SET_SUCCESS:
                    return {
                        isLoading: false,
                        isSuccess: true,
                        data: action.payload.payload
                    }
                case SET_ERROR:
                    return {
                        isLoading: false,
                        isSuccess: false,
                        data: null
                    }
                default:
                    return state;
            }
        }
    }
});

export const { setStateDetailOfContact } = detailOfContactSlice.actions;

export default detailOfContactSlice.reducer;