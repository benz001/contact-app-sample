import { createSlice } from "@reduxjs/toolkit";
import { SET_ERROR, SET_LOADING, SET_SUCCESS } from "../helper/RequestState";

const deleteContactSlice = createSlice({
    name: 'deleteContact',
    initialState: {
        isLoading: false,
        isSuccess: false,
        data: null
    },
    reducers: {
        setStateDeleteContact: (state, action) => {
            switch (action.payload.type) {
                case SET_LOADING:
                    return {
                        isLoading: true,
                        isSuccess: false,
                        data: null
                    }
                case SET_SUCCESS:
                    return {
                        isLoading: false,
                        isSuccess: true,
                        data: action.payload.payload
                    }
                case SET_ERROR:
                    return {
                        isLoading: false,
                        isSuccess: false,
                        data: null
                    }
                default:
                    return state;
            }
        }
    }
});

export const { setStateDeleteContact } = deleteContactSlice.actions;


export default deleteContactSlice.reducer;