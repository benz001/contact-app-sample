import { createSlice } from "@reduxjs/toolkit";

export const SET_VALUE = 'SET_VALUE';
export const CLEAR_VALUE = 'SET_VALUE';


const showDeleteVisibilitySlice = createSlice({
    name: 'showDeleteVisibility',
    initialState: {
        isShow: false,
        id: ''
    },
    reducers: {
        setStateShowDeleteVisibility: (state, action) => {
            console.log(`setStateShowDeleteVisibility: ${JSON.stringify(action.payload)}`);
            switch (action.payload.type) {
                case SET_VALUE:
                    return {
                        isShow: action.payload.payload?.isShow,
                        id: action.payload.payload?.id
                    }
                case CLEAR_VALUE:
                    return {
                        isShow: false,
                        id: ''
                    }
                default:
                    return state;
            }
        }
    }
});

export const { setStateShowDeleteVisibility } = showDeleteVisibilitySlice.actions;


export default showDeleteVisibilitySlice.reducer;