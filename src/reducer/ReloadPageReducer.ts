import { createSlice } from "@reduxjs/toolkit";

export const SET_RELOAD = 'SET_VALUE';


const reloadPageSlice = createSlice({
    name: 'reloadPage',
    initialState: {
        isReload: false,
       
    },
    reducers: {
        setStateReloadPage: (state, action) => {
            console.log(`setStateReloadPage: ${JSON.stringify(action.payload)}`);
            switch (action.payload.type) {
                case SET_RELOAD:
                    return {
                        isReload: action.payload.payload,
                       
                    }
                default:
                    return state;
            }
        }
    }
});

export const { setStateReloadPage } = reloadPageSlice.actions;


export default reloadPageSlice.reducer;