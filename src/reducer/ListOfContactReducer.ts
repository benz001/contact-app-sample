import { createSlice } from "@reduxjs/toolkit";
import { SET_ERROR, SET_LOADING, SET_SUCCESS } from "../helper/RequestState";

const listOfContactSlice = createSlice({
    name: 'listOfcontact',
    initialState: {
        isLoading: false,
        isSuccess: false,
        data: []
    },
    reducers: {
        setStateListOfContact: (state, action) => {
            switch (action.payload.type) {
                case SET_LOADING:
                    return {
                        isLoading: true,
                        isSuccess: false,
                        data: []
                    }
                case SET_SUCCESS:
                    return {
                        isLoading: false,
                        isSuccess: true,
                        data: action.payload.payload
                    }
                case SET_ERROR:
                    return {
                        isLoading: false,
                        isSuccess: false,
                        data: []
                    }
                default:
                    return state;
            }
        }
    }
});

export const { setStateListOfContact } = listOfContactSlice.actions;


export default listOfContactSlice.reducer;