import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import ListOfContactPage from "../ui/ListOfContactPage";
import CreateContactPage from "../ui/CreateContactPage";
import UpdateContactPage from "../ui/UpdateContactPage";
import DetailOfContactPage from "../ui/DetailOfContactPage";


export type RootStackParamList = {
    ListOfContact: undefined,
    CreateContact: undefined,
    UpdateContact: {
        param: any
    },
    DetailOfContact: {
        param: any
    }
};

const Stack = createNativeStackNavigator<RootStackParamList>();


export default function AppNavigation() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="ListOfContact">
                <Stack.Screen name="ListOfContact" component={ListOfContactPage} options={{headerShown: false}}/>
                <Stack.Screen name="CreateContact" component={CreateContactPage} options={{headerShown: false}}/>
                <Stack.Screen name="UpdateContact" component={UpdateContactPage} options={{headerShown: false}}/>
                <Stack.Screen name="DetailOfContact" component={DetailOfContactPage} options={{headerShown: false}}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}


