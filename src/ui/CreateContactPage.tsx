import { useState } from "react";
import ContactAPIServices from "../service/ContactAPIServices";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store/Store";
import { SET_ERROR, SET_LOADING, SET_SUCCESS } from "../helper/RequestState";
import { ActivityIndicator, Alert, Text, TouchableOpacity, View } from "react-native";
import { ConvertStandardResponse, StandardResponse } from "../model/StandardResponse";
import AppBarPaper from "../reusable_component/AppBar";
import Spacer from "../reusable_component/spacer";
import { TextInput, useTheme } from "react-native-paper";
import CreateContactStyles from "./styles/CreateContactStyles";
import RequestBody from "../helper/RequestBody";
import { setStateCreateContact } from "../reducer/CreateContactReducer";
import { RootStackParamList } from "../navigation/AppNavigation";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { SET_RELOAD, setStateReloadPage } from "../reducer/ReloadPageReducer";


type CreateContactProps = NativeStackScreenProps<RootStackParamList, 'CreateContact'>;


export default function CreateContactPage({ navigation }: CreateContactProps) {
    const contactAPIService = new ContactAPIServices();
    const state = useSelector((state: RootState) => state.createContact);
    const dispatch = useDispatch();


    const [requestBody, setRequestBody] = useState<RequestBody>({
        firstName: '',
        lastName: '',
        age: 0,
        photo: ''
    })


    const createContactProcess = async () => {
        if (requestBody.firstName === '' || requestBody.lastName === '' || requestBody.age === 0 || requestBody.photo === '') return Alert.alert('Message', 'Text Input can not empty');
        console.log(`request body: ${JSON.stringify(requestBody)}`);
        console.log('createContactProcess loading');
        dispatch(setStateCreateContact({ type: SET_LOADING, payload: null }));
        try {
            const response = await contactAPIService.createContact({ body: requestBody });
            console.log(`response of createContactProcess: ${JSON.stringify(response)}`);
            if (response.message === 'Success!') {
                console.log('createBookProcess success');
                dispatch(setStateCreateContact({ type: SET_SUCCESS, payload: response.data }));
                dispatch(setStateReloadPage({ type: SET_RELOAD, payload: true }));
                return Alert.alert('Message', `${response.message}`, [
                    {
                        text: 'Ok',
                        onPress: () => navigation.pop(),
                    },
                ]);
            } else {
                console.log(`createBookProcess bad: ${response.message}`);
                dispatch(setStateCreateContact({ type: SET_ERROR, payload: null }));
                return Alert.alert('Message', `Add Contact Bad\n${response.message}}`);

            }


        } catch (error: any) {
            console.log(`createBookProcess error: ${JSON.stringify(error?.response?.message)}`);
            dispatch(setStateCreateContact({ type: SET_ERROR, payload: null }));
            if (error?.response == undefined) {
                return Alert.alert('Message', 'Network is error');
            }
            const errorResponse: StandardResponse = ConvertStandardResponse.toModel(JSON.stringify(error));
            return Alert.alert('Message', `Add Contact Error\n${errorResponse.message}`);
        }
    }

    const updateField = <K extends keyof RequestBody>(field: K, value: RequestBody[K]) => {
        setRequestBody((prevState) => ({
            ...prevState,
            [field]: value
        }))
    }


    return (
        <View style={CreateContactStyles.container}>
            <AppBarPaper title="Add Contact" showBack backPress={() => navigation.pop()} />
            <Spacer width={0} height={20} />
            <View style={CreateContactStyles.content}>
                <CreateContactFormComponent onChangeFirstName={(v) => updateField('firstName', v)} onChangeLastName={(v) => updateField('lastName', v)} onChangeAge={(v) => updateField('age', Number(v == '' ? '0' : v))} onChangePhotoUrl={(v) => updateField('photo', v)} />
                <Spacer width={0} height={20} />
                <CreateBookButtonComponent onPress={() => createContactProcess()} isLoading={state.isLoading} />
            </View>
        </View>
    );
}

function CreateContactFormComponent({ onChangeFirstName, onChangeLastName, onChangeAge, onChangePhotoUrl }: { onChangeFirstName: (v: string) => void, onChangeLastName: (v: string) => void, onChangeAge: (v: string) => void, onChangePhotoUrl: (v: string) => void }) {
    return (
        <View style={CreateContactStyles.form}>
            <TextInput placeholder="Enter First Name" mode="outlined" onChangeText={onChangeFirstName} />
            <Spacer width={0} height={10} />
            <TextInput placeholder="Enter Last Name" mode="outlined" onChangeText={onChangeLastName} />
            <Spacer width={0} height={10} />
            <TextInput placeholder="Enter Age" mode="outlined" onChangeText={onChangeAge} keyboardType="numeric" />
            <Spacer width={0} height={10} />
            <TextInput placeholder="Enter Photo URL" mode="outlined" onChangeText={onChangePhotoUrl} />
        </View>
    );
}

function CreateBookButtonComponent({ onPress, isLoading }: { onPress: () => void, isLoading: boolean }) {
    console.log(`is loading button: ${isLoading}`);
    if (isLoading) {
        return (
            <TouchableOpacity style={CreateContactStyles.buttonAdd}>
                <ActivityIndicator color='white' size={'large'} style={{ margin: 'auto' }} />
            </TouchableOpacity>
        );
    }
    return (
        <TouchableOpacity onPress={onPress} style={CreateContactStyles.buttonAdd}>
            <Text style={{ color: 'white', fontSize: 15, margin: 'auto' }}>ADD CONTACT</Text>
        </TouchableOpacity>
    );
}