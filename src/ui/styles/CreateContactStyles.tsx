import { StyleSheet } from "react-native";

const CreateContactStyles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        flexDirection: 'column',
    },
    content: {
        width: '100%',
        height: '100%',
        paddingStart: 20,
        paddingEnd: 20
    },
    titleLabel: {
        fontSize: 20,
        textDecorationLine: 'underline'
    },
    form: {
        width: '100%',
        flexDirection: 'column'
    },
    textInput: {
        backgroundColor: 'white'
    },
    buttonAdd: {
        width: '100%',
        height: 40,
        backgroundColor: 'rgb(120, 69, 172)'
    },
});

export default CreateContactStyles;