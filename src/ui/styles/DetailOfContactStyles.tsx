import { StyleSheet } from "react-native";
import { useTheme } from "react-native-paper";



const DetailOfContactStyles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        flexDirection: 'column',
    },
    content: {
        width: '100%',
        height: '100%',
        paddingStart: 20,
        paddingEnd: 20
    },
    titleLabel: {
        fontSize: 20,
        textDecorationLine: 'underline'
    },
    list: {
        width: '100%',
        flex: 1,
        marginBottom: 100
    },
    plusContainer:{
        width: 50,
        height: 50,
        backgroundColor: 'rgb(120, 69, 172)',
        borderRadius: 50/2,
        position: 'absolute',
        bottom: 0,
        right: 0,
        marginEnd: 5,
        marginBottom: 5
        
    },
    plusLabel: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        margin: 'auto'
    }
});

export default DetailOfContactStyles;