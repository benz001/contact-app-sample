import { useDispatch, useSelector } from "react-redux";
import ContactAPIServices from "../service/ContactAPIServices";
import { AppDispatch, RootState } from "../store/Store";
import { useEffect, useState } from "react";
import { Card, useTheme, Button } from "react-native-paper";
import { setStateDetailOfContact } from "../reducer/DetailOfContactReducer";
import { SET_ERROR, SET_LOADING, SET_SUCCESS } from "../helper/RequestState";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootStackParamList } from "../navigation/AppNavigation";
import { ActivityIndicator, Text, TouchableOpacity, View } from "react-native";
import Spacer from "../reusable_component/spacer";
import AppBarPaper from "../reusable_component/AppBar";
import { DataOfContact } from "../model/DetailOfContactResponse";
import DetailOfContactStyles from "./styles/DetailOfContactStyles";
import { setStateReloadPage } from "../reducer/ReloadPageReducer";

type DetailOfBookProps = NativeStackScreenProps<RootStackParamList, 'DetailOfContact'>;


export default function DetailOfContactPage({ navigation, route }: DetailOfBookProps) {
    const contactAPIService = new ContactAPIServices();
    const state = useSelector((state: RootState) => state.detailOfContact);
    const stateReloadPage = useSelector((state: RootState) => state.reloadPage);

    const dispatch: AppDispatch = useDispatch();
    const [resfreshPage, setRefreshPage] = useState(false);
    const theme = useTheme();
    const param = route.params?.param as DataOfContact;


    useEffect(() => {
        console.log(`id: ${param.id}`)
        getDetailOfContactProcess();
    }, []);


    const getDetailOfContactProcess = async () => {
        setRefreshPage(true);
        console.log('getDetailOfContactProcess loading');
        dispatch(setStateDetailOfContact({ type: SET_LOADING, payload: null }));
        try {
            const response = await contactAPIService.detailOfContact({
                id: param.id
            });
            if (response.data.id != null) {
                console.log('getDetailOfContactProcess success');
                dispatch(setStateDetailOfContact({ type: SET_SUCCESS, payload: response.data }));
            } else {
                console.log('getDetailOfContactProcess bad');
                dispatch(setStateDetailOfContact({ type: SET_ERROR, payload: null }));
            }
            setRefreshPage(false);
        } catch (error: any) {
            setRefreshPage(false);
            console.log(`getDetailOfContactProcess error: ${error}`);
            dispatch(setStateDetailOfContact({ type: SET_ERROR, payload: null }));
        }
    }



    console.log(`isReload: ${stateReloadPage.isReload}`);
    console.log(`state data detail contact: ${state.data}`);

    return (

        <View style={DetailOfContactStyles.container}>
            <AppBarPaper title="Detail Of Contact" showBack backPress={()=> navigation.pop()}/>
            {!state.isLoading ? <Spacer width={0} height={20} /> : null}
            <View style={DetailOfContactStyles.content}>

                {
                    state.isLoading ?
                        <ActivityIndicator color={theme.colors.primary} size={'large'} style={{ margin: 'auto' }} /> :
                        state.isSuccess ? <ItemOfContactComponent item={state.data!} /> : <Button mode="contained" onPress={() => {
                            console.log('reload');
                            getDetailOfContactProcess();
                        }} style={{ margin: 'auto' }}><Text>Reload</Text></Button>
                }
            </View>
        </View>
    );



}


function ItemOfContactComponent({ item }: { item: DataOfContact }) {
    return (
        <Card style={{ marginTop: 20 }}>
            {
                item.photo == null || item.photo === '' ? <View style={{ width: '100%', height: 200, backgroundColor: 'lightgrey' }} /> : <Card.Cover source={{ uri: `${item.photo}` }} style={{ resizeMode: 'cover' }} />
            }

            <Card.Title title={`${item.firstName} ${item.lastName}`} />
            <Card.Content>
                <Text style={{ fontSize: 12 }}>Age: {item.age}</Text>
            </Card.Content>

        </Card>
    );
}