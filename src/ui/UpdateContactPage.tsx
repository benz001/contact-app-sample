import ContactAPIServices from "../service/ContactAPIServices";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store/Store";
import { SET_ERROR, SET_LOADING, SET_SUCCESS } from "../helper/RequestState";
import { ActivityIndicator, Alert, Text, TouchableOpacity, View } from "react-native";
import { ConvertStandardResponse, StandardResponse } from "../model/StandardResponse";
import AppBarPaper from "../reusable_component/AppBar";
import Spacer from "../reusable_component/spacer";
import { TextInput, useTheme } from "react-native-paper";
import RequestBody from "../helper/RequestBody";
import UpdateContactStyles from "./styles/UpdateContactStyles";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootStackParamList } from "../navigation/AppNavigation";
import { DataOfContact } from "../model/ListOfContactResponse";
import { setStateUpdateContact } from "../reducer/UpdateContactReducer";
import { useState } from "react";


type UpdateContactProps = NativeStackScreenProps<RootStackParamList, 'UpdateContact'>;


export default function UpdateContactPage({ navigation, route }: UpdateContactProps) {
    const contactAPIService = new ContactAPIServices();
    const state = useSelector((state: RootState) => state.updateContact);
    const dispatch = useDispatch();
    const theme = useTheme();


    const param = route.params?.param as DataOfContact;

    const [requestBody, setRequestBody] = useState<RequestBody>({
        firstName: param.firstName,
        lastName: param.lastName,
        age: param.age,
        photo: param.photo
    })

    const updateContactProcess = async () => {
        if (requestBody.firstName === '' || requestBody.lastName === '' || requestBody.age === 0 || requestBody.photo === '') return Alert.alert('Message', 'Text Input can not empty')
        console.log('updateContactProcess loading');
        dispatch(setStateUpdateContact({ type: SET_LOADING, payload: null }));
        try {
            const response = await contactAPIService.updateContact({ id: param.id, body: requestBody });
            if (response.message === 'Success!') {
                console.log('updateContactProcess success');
                dispatch(setStateUpdateContact({ type: SET_SUCCESS, payload: response.data }));
                return Alert.alert('Message', `${response.message}`, [
                    {
                        text: 'Ok',
                        onPress: () => navigation.pop(),
                    },
                ]);
            } else {
                console.log('updateContactProcess bad');
                dispatch(setStateUpdateContact({ type: SET_ERROR, payload: null }));
                return Alert.alert('Message', `Update Contact Bad\n${response.message}`);
            }


        } catch (error: any) {
            console.log(`updateBookProcess error: ${JSON.stringify(error?.response?.message)}`);
            dispatch(setStateUpdateContact({ type: SET_ERROR, payload: null }));
            if (error?.response == undefined) {
                return Alert.alert('Message', 'Network is error');
            }
            const errorResponse: StandardResponse = ConvertStandardResponse.toModel(JSON.stringify(error));
            return Alert.alert('Message', `Update Contact Error\n${errorResponse.message}`);
        }
    }

    const updateField = <K extends keyof RequestBody>(field: K, value: RequestBody[K]) => {
        setRequestBody((prevState) => ({
            ...prevState,
            [field]: value
        }))
    }



    return (
        <View style={UpdateContactStyles.container}>
            <AppBarPaper title="Update Contact" showBack backPress={() => navigation.pop()} />
            <Spacer width={0} height={20} />
            <View style={UpdateContactStyles.content}>
                <UpdateContactFormComponent onChangeFirstName={(v) => updateField('firstName', v)} valueFirstName={requestBody.firstName} onChangeLastName={(v) => updateField('lastName', v)} valueLastName={requestBody.lastName} onChangeAge={(v) => updateField('age', Number(v == '' ? '0' : v))} valueAge={`${requestBody.age == 0? '': requestBody.age}`} onChangePhotoUrl={(v) => updateField('photo', v)} valuePhotoUrl={requestBody.photo} />
                <Spacer width={0} height={20} />
                <UpdateContactButtonComponent isLoading={state.isLoading} onPress={() => updateContactProcess()} />
            </View>
        </View>
    );
}

function UpdateContactFormComponent({ onChangeFirstName, valueFirstName, onChangeLastName, valueLastName, onChangeAge, valueAge, onChangePhotoUrl, valuePhotoUrl }: { onChangeFirstName: (v: string) => void, valueFirstName: string, onChangeLastName: (v: string) => void, valueLastName: string, onChangeAge: (v: string) => void, valueAge: string, onChangePhotoUrl: (v: string) => void, valuePhotoUrl: string }) {
    return (
        <View style={UpdateContactStyles.form}>
            <TextInput placeholder="Enter First Name" value={valueFirstName} mode="outlined" onChangeText={onChangeFirstName} />
            <Spacer width={0} height={10} />
            <TextInput placeholder="Enter Last Name" value={valueLastName} mode="outlined" onChangeText={onChangeLastName} />
            <Spacer width={0} height={10} />
            <TextInput placeholder="Enter Age" mode="outlined" value={valueAge} onChangeText={onChangeAge} keyboardType="numeric" />
            <Spacer width={0} height={10} />
            <TextInput placeholder="Enter Photo URL" mode="outlined" value={valuePhotoUrl} onChangeText={onChangePhotoUrl} />
        </View>
    );
}

function UpdateContactButtonComponent({ onPress, isLoading }: { onPress: () => void, isLoading: boolean }) {
    console.log(`is loading button: ${isLoading}`);
    if (isLoading) {
        return (
            <TouchableOpacity style={UpdateContactStyles.buttonUpdate}>
                <ActivityIndicator color='white' size={'large'} style={{ margin: 'auto' }} />
            </TouchableOpacity>);
    }
    return (
        <TouchableOpacity onPress={onPress} style={UpdateContactStyles.buttonUpdate}>
            <Text style={{ color: 'white', fontSize: 15, margin: 'auto' }}>UPDATE CONTACT</Text>
        </TouchableOpacity>
    );
}