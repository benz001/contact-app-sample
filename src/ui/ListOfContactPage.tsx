import { useCallback, useEffect, useState } from "react";
import ContactAPIServices from "../service/ContactAPIServices";
import { ConvertListOfContactResponse, DataOfContact } from "../model/ListOfContactResponse";
import { useSelector, useDispatch } from 'react-redux';
import { NativeStackNavigationProp, NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootStackParamList } from "../navigation/AppNavigation";
import { SET_ERROR, SET_LOADING, SET_SUCCESS } from "../helper/RequestState";
import { ActivityIndicator, Alert, FlatList, RefreshControl, Text, TouchableOpacity, View } from "react-native";
import { AppDispatch, RootState } from "../store/Store";
import AppBarPaper from "../reusable_component/AppBar";
import Spacer from "../reusable_component/spacer";
import MultipleDialogPaper from "../reusable_component/DialogPaper";
import { Button, Card, useTheme } from "react-native-paper";
import ListOfContactStyles from "./styles/ListOfContactStyles";
import { CLEAR_VALUE, SET_VALUE, setStateShowDeleteVisibility } from "../reducer/ShowDeleteVisibilityReducer";
import { setStateListOfContact } from "../reducer/ListOfContactReducer";
import { setStateDeleteContact } from "../reducer/DeleteContactReducer";
import { ConvertStandardResponse, StandardResponse } from "../model/StandardResponse";
import { SET_RELOAD, setStateReloadPage } from "../reducer/ReloadPageReducer";
import { useFocusEffect } from "@react-navigation/native";

type ListOfBookProps = NativeStackScreenProps<RootStackParamList, 'ListOfContact'>;


export default function ListOfContactPage({ navigation }: ListOfBookProps) {
    const contactAPIService = new ContactAPIServices();
    const stateListOfContact = useSelector((state: RootState) => state.listOfcontact);
    const stateShowDeleteVisibility = useSelector((state: RootState) => state.showDeleteVisibility);
    const stateReloadPage = useSelector((state: RootState) => state.reloadPage);

    const dispatch: AppDispatch = useDispatch();
    const [resfreshPage, setRefreshPage] = useState(false);
    const theme = useTheme();


    useEffect(() => {
        getListOfContactProcess();
    }, []);

    useFocusEffect(
        useCallback(() => {
            console.log(`is reload callback: ${stateReloadPage.isReload}`);
                getListOfContactProcess();
        }, [])
    );

    const getListOfContactProcess = async () => {
        setRefreshPage(true);
        console.log('getListOfContactProcess loading');
        dispatch(setStateListOfContact({ type: SET_LOADING, payload: null }));
        try {
            const response = await contactAPIService.getListOfContact();
            if (response.data.length > 0) {
                console.log('getListOfContactProcess success');
                dispatch(setStateListOfContact({ type: SET_SUCCESS, payload: response.data }));
            } else {
                console.log('getListOfContactProcess bad');
                dispatch(setStateListOfContact({ type: SET_ERROR, payload: [] }));
            }
            setRefreshPage(false);
        } catch (error: any) {
            setRefreshPage(false);
            console.log(`getListOfContactProcess error: ${error}`);
            dispatch(setStateListOfContact({ type: SET_ERROR, payload: null }));
        }
    }

    const deleteContactProcess = async (id: string) => {
        dispatch(setStateShowDeleteVisibility({
            type: SET_VALUE, payload: {
                isShow: false,
                id: id
            }
        }));
        console.log(`deleteContactProcess id: ${id}`);
        setRefreshPage(true);
        console.log('deleteContactProcess loading');
        dispatch(setStateDeleteContact({ type: SET_LOADING, payload: null }));
        try {
            const response = await contactAPIService.deleteContact({ id });
            if (response.message === 'Success!') {
                getListOfContactProcess();
            } else {
                console.log('deleteBookProcess bad');
                dispatch(setStateDeleteContact({ type: SET_ERROR, payload: [] }));
                return Alert.alert('Message', `Delete Contact Error\n${response.message}`);

            }
            setRefreshPage(false);
        } catch (error: any) {
            setRefreshPage(false);
            console.log(`deleteContactProcess error: ${JSON.stringify(error?.response?.message)}`);
            if (error?.response == undefined) {
                return Alert.alert('Message', 'Network is error');
            }
            const errorResponse: StandardResponse = ConvertStandardResponse.toModel(JSON.stringify(error));
            dispatch(setStateDeleteContact({ type: SET_ERROR, payload: errorResponse.message }));
            return Alert.alert('Message', `Delete Contact Error\n${errorResponse.message}`);
        }
    }

    console.log(`isReload: ${stateReloadPage.isReload}`);

    return (

        <View style={ListOfContactStyles.container}>
            <AppBarPaper title="List Of Contact" />
            {!stateListOfContact.isLoading ? <Spacer width={0} height={20} /> : null}
            <View style={ListOfContactStyles.content}>

                {
                    stateListOfContact.isLoading ?
                        <ActivityIndicator color={theme.colors.primary} size={'large'} style={{ margin: 'auto' }} /> :
                        stateListOfContact.isSuccess ? <ListOfContactComponent navigation={navigation} list={stateListOfContact.data} refresh={resfreshPage} onRefresh={() => getListOfContactProcess()}
                            dispatchState={dispatch} /> : <Button mode="contained" onPress={() => {
                                console.log('reload');
                                getListOfContactProcess();
                            }} style={{ margin: 'auto' }}><Text>Reload</Text></Button>
                }
            </View>
            <TouchableOpacity onPress={() =>{
                 navigation.navigate('CreateContact');
                 dispatch(setStateReloadPage({type: SET_RELOAD, payload: false}));
                 }} style={ListOfContactStyles.plusContainer}>
                <Text style={ListOfContactStyles.plusLabel}>+</Text>
            </TouchableOpacity>
            <MultipleDialogPaper
                visible={stateShowDeleteVisibility.isShow}

                onDismiss={() =>
                    dispatch(setStateShowDeleteVisibility({
                        type: CLEAR_VALUE
                    }))
                }
                title="Message"
                body="Are You sure to delete this item ?"
                onPressOk={async () => {
                    await deleteContactProcess(stateShowDeleteVisibility.id);

                }}
                onPressCancel={() => {
                    console.log('cancel');
                    dispatch(setStateShowDeleteVisibility({
                        type: CLEAR_VALUE
                    }))
                }}
            />
        </View>
    );



}

function ListOfContactComponent({ list, refresh, onRefresh, navigation, dispatchState }: { list: DataOfContact[], refresh: boolean, onRefresh: () => void, navigation: NativeStackNavigationProp<RootStackParamList, 'ListOfContact', undefined>, dispatchState: AppDispatch }) {
    if (list.length == 0) {
        return (
            <View style={{ margin: 'auto', justifyContent: 'center', alignItems: 'center' }}>
                <Text>Not Found</Text>
                <TouchableOpacity onPress={onRefresh}>
                    <Text>(Please Tap To Reload)</Text>
                </TouchableOpacity>
            </View>

        );
    }
    return (
        <FlatList
            style={ListOfContactStyles.list}
            data={list}
            refreshControl={
                <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
            }
            keyExtractor={(item, index) => `${item.id}`}
            renderItem={(item) => <ItemOfContactComponent item={item.item} onPressUpdate={()=>{
                dispatchState(setStateReloadPage({type: SET_RELOAD, payload: false}));
                navigation.navigate('UpdateContact', { param: item.item });
            }} onPressDelete={() => dispatchState(setStateShowDeleteVisibility({
                type: SET_VALUE, payload: {
                    isShow: true,
                    id: item.item.id
                }
            }))} onDetail={()=> navigation.navigate('DetailOfContact', { param: item.item })} />}
        />
    );
}

function ItemOfContactComponent({ item, onPressUpdate, onPressDelete, onDetail }: { item: DataOfContact, onPressUpdate: () => void, onPressDelete: () => void, onDetail: () => void }) {
    return (
        <Card style={{ marginTop: 20 }}>
            {
                item.photo === null || item.photo === ''?
                <View style={{ width: '100%', height: 200, backgroundColor: 'lightgrey', borderTopRightRadius: 10, borderTopLeftRadius: 10 }} >
                    <Text style={{margin: 'auto', textAlign: 'center'}}>Image Not Found</Text>
                </View> : <Card.Cover source={{ uri: `${item.photo}` }} style={{ resizeMode: 'cover' }} />
            }

            <Card.Title title={`${item.firstName} ${item.lastName}`} />
            <Card.Content>
                <Text style={{ fontSize: 12 }}>Age: {item.age}</Text>
            </Card.Content>
            <Card.Actions>
                <Button onPress={onPressUpdate}>Update</Button>
                <Button onPress={onPressDelete}>Delete</Button>
                <Button onPress={onDetail}>Detail</Button>
            </Card.Actions>
        </Card>
    );
}