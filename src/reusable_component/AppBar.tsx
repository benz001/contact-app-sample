import { View } from "react-native";
import { Appbar, useTheme } from "react-native-paper"

const AppBarPaper = ({ backPress, showBack, title, actions }: AppbarProps) => {
    const theme = useTheme();

    return (
        <Appbar.Header style={{backgroundColor: theme.colors.elevation.level2, paddingHorizontal: 15}}>
            {(showBack ?? false) ? <Appbar.BackAction onPress={backPress} /> : <View />}
            
            <Appbar.Content   title={title} />
            {
                actions !== undefined ?
                    actions.map((item, index) => <Appbar.Action icon={item.icon} onPress={item.onPress} />) : <View />
            }
        </Appbar.Header>

    );
}

export default AppBarPaper;

interface AppbarProps {
    showBack?: boolean;
    backPress?: () => void;
    title: string;
    actions?: Array<AppbarActionProps>

}

interface AppbarActionProps {
    icon: string;
    onPress: () => void;
}