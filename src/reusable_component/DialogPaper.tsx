import { Button, Dialog, Text } from "react-native-paper";

const MultipleDialogPaper = ({visible, onDismiss, title, body, onPressOk, onPressCancel}: {visible: boolean, onDismiss: () => void, title: string, body: string, onPressOk: () => void, onPressCancel: ()=>void}) => {
    return (
        <Dialog visible={visible} onDismiss={onDismiss}>
            <Dialog.Title>{title}</Dialog.Title>
            <Dialog.Content>
              <Text variant="bodyMedium">{body}</Text>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={onPressOk}>OK</Button>
              <Button onPress={onPressCancel}>Cancel</Button>
            </Dialog.Actions>
          </Dialog>
    );
} 

export default MultipleDialogPaper;