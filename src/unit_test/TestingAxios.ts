// src/services/__tests__/ContactAPIServices.test.ts
import axios from 'axios';
import { ConvertListOfContactResponse } from '../model/ListOfContactResponse';
import baseURL from '../helper/BaseURL';
import { ConvertDetailOfContactResponse } from '../model/DetailOfContactResponse';
import ContactAPIServices from '../service/ContactAPIServices';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('ContactAPIServices', () => {
  let contactAPIService: ContactAPIServices;

  beforeEach(() => {
    contactAPIService = new ContactAPIServices();
  });

  it('should fetch list of contacts', async () => {
    const mockData = { /* mock data for ListOfContactResponse */ };
    mockedAxios.get.mockResolvedValueOnce({ data: mockData });

    const result = await contactAPIService.getListOfContact();
    expect(result).toEqual(ConvertListOfContactResponse.toModel(JSON.stringify(mockData)));
    expect(mockedAxios.get).toHaveBeenCalledWith(`${baseURL}/contact`);
  });

  it('should fetch detail of a contact', async () => {
    const mockData = { /* mock data for StandardResponse */ };
    mockedAxios.get.mockResolvedValueOnce({ data: mockData });

    const result = await contactAPIService.detailOfContact({ id: '93ad6070-c92b-11e8-b02f-cbfa15db428b' });
    expect(result).toEqual(ConvertDetailOfContactResponse.toModel(JSON.stringify(mockData)));
    expect(mockedAxios.get).toHaveBeenCalledWith(`${baseURL}/contact/123`);
  });

  it('should create a contact', async () => {
    const mockData = { status: 201, data: {} };
    mockedAxios.post.mockResolvedValueOnce(mockData);

    const result = await contactAPIService.createContact({ body: {} });
    expect(result).toEqual({ data: null, message: 'Success!' });
    expect(mockedAxios.post).toHaveBeenCalledWith(`${baseURL}/contact`, {});
  });

  it('should update a contact', async () => {
    const mockData = { status: 200, data: {} };
    mockedAxios.put.mockResolvedValueOnce(mockData);

    const result = await contactAPIService.updateContact({ id: '93ad6070-c92b-11e8-b02f-cbfa15db428b', body: {} });
    expect(result).toEqual({ data: null, message: 'Success!' });
    expect(mockedAxios.put).toHaveBeenCalledWith(`${baseURL}/contact/93ad6070-c92b-11e8-b02f-cbfa15db428b`, {});
  });

  it('should delete a contact', async () => {
    const mockData = { status: 200, data: {} };
    mockedAxios.delete.mockResolvedValueOnce(mockData);

    const result = await contactAPIService.deleteContact({ id: '93ad6070-c92b-11e8-b02f-cbfa15db428b' });
    expect(result).toEqual({ data: null, message: 'Success!' });
    expect(mockedAxios.delete).toHaveBeenCalledWith(`${baseURL}/contact/93ad6070-c92b-11e8-b02f-cbfa15db428b`);
  });
});

