import axios from "axios";
import baseURL from "../helper/BaseURL";
import { ConvertListOfContactResponse, ListOfContactResponse } from "../model/ListOfContactResponse";
import { ConvertDetailOfContactResponse } from "../model/DetailOfContactResponse";
import { ConvertStandardResponse, StandardResponse } from "../model/StandardResponse";

export default class ContactAPIServices {
    public async getListOfContact(): Promise<ListOfContactResponse> {
        const response = await axios.get(`${baseURL}/contact`);
        return ConvertListOfContactResponse.toModel(JSON.stringify(response.data));
    }

    public async detailOfContact({ id }: { id: string }): Promise<StandardResponse> {
        const response = await axios.get(`${baseURL}/contact/${id}`);
        return ConvertDetailOfContactResponse.toModel(JSON.stringify(response.data));
    }

    public async createContact({ body }: { body: object }):Promise<StandardResponse>  {
        const response = await axios.post(`${baseURL}/contact`, body);
        if (response.status == 200 || response.status == 201) {
            const successResponse: StandardResponse = {
                data: null,
                message: 'Success!',
            }

            return successResponse;

        } else {
            const erroResponse: StandardResponse = {
                data: null,
                message: 'Failed',
            }

            return erroResponse;
        }
    }

    public async updateContact({ id, body }: { id: string, body: object}):Promise<StandardResponse> {
        const response = await axios.put(`${baseURL}/contact/${id}`, body);
        if (response.status == 200 || response.status == 201) {
            const successResponse: StandardResponse = {
                data: null,
                message: 'Success!',
            }

            return successResponse;

        } else {
            const erroResponse: StandardResponse = {
                data: null,
                message: 'Failed',
            }

            return erroResponse;
        }
    }

    public async deleteContact({ id }: { id: string }): Promise<StandardResponse> {
        const response = await axios.delete(`${baseURL}/contact/${id}`);
        if (response.status == 200 || response.status == 201) {
            const successResponse: StandardResponse = {
                data: null,
                message: 'Success!',
            }
            return successResponse;

        } else {
            const erroResponse: StandardResponse = {
                data: null,
                message: 'Failed',
            }

            return erroResponse;
        }
    }
}