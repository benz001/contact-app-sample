type ActionOfReducer = { type: 'SET_LOADING', payload: any } | { type: 'SET_SUCCESS', payload: any } | { type: 'SET_ERROR', payload: any };

export default ActionOfReducer;