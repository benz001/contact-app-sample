interface StateObjOfResponse {
    isLoading: boolean;
    isSuccess: boolean;
    data: any;
}

export default StateObjOfResponse;