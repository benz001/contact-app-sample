interface RequestBody {
    firstName: string,
    lastName: string,
    age: number,
    photo: string

}

export default RequestBody;