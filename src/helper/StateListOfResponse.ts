interface StateListOfResponse {
    isLoading: boolean;
    isSuccess: boolean;
    data: Array<any>;
}

export default StateListOfResponse;