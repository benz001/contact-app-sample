/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import type { PropsWithChildren } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import { Provider } from 'react-redux';
import store from './src/store/Store';
import AppNavigation from './src/navigation/AppNavigation';




function App(): React.JSX.Element {
  console.log('testingerssssss');
  return (
    <Provider store={store}>
        <AppNavigation/>
    </Provider>

  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'lightgrey'
  },
  text: {
    textAlign: 'center',
    margin: 'auto'

  }
});

export default App;
